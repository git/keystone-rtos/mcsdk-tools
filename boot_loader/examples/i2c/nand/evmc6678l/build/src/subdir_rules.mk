################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/i2c_boot_nand_example.obj: ../../../../../examples/i2c/nand/src/i2c_boot_nand_example.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)" --include_path="../../../../../examples/i2c/nand/include" --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/i2c_boot_nand_example.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


