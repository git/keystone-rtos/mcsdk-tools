export CGT_INSTALL_DIR=${C6X_GEN_INSTALL_PATH}
export TARGET=k2h
export ENDIAN=little

echo CGT_INSTALL_DIR set as: ${CGT_INSTALL_DIR}
echo TARGET set as: ${TARGET}

echo Converting .out to HEX ...
if [ ${ENDIAN} == little ]
then
${CGT_INSTALL_DIR}/bin/hex6x -order L pcieboot_ddrinit.rmd pcieboot_ddrinit_evm${TARGET}.out
else
${CGT_INSTALL_DIR}/bin/hex6x -order M pcieboot_ddrinit.rmd pcieboot_ddrinit_evm${TARGET}.out
fi

../../../../../../bttbl2hfile/Bttbl2Hfile pcieboot_ddrinit.btbl pcieboot_ddrinit.h pcieboot_ddrinit.bin

../../../../../../hfile2array/hfile2array pcieboot_ddrinit.h pcieDdrInit.h ddrInitCode

mv pcieDdrInit.h ../../../linux_host_loader/pcieDdrInit_${TARGET}.h
