set C6000_CG_DIR=%C6X_GEN_INSTALL_PATH%
set TARGET=k2h
set ENDIAN=little
set PATH=%PATH%;%SystemRoot%\system32;%SystemRoot%;


@echo off

echo C6000_CG_DIR set as: %C6000_CG_DIR%
echo TARGET set as: %TARGET%

echo Converting .out to HEX ...
if %ENDIAN% == little (
%C6000_CG_DIR%\bin\hex6x -order L pcieboot_ddrinit.rmd pcieboot_ddrinit_evm%TARGET%.out
) else (
%C6000_CG_DIR%\bin\hex6x -order M pcieboot_ddrinit.rmd pcieboot_ddrinit_evm%TARGET%.out
)

..\..\..\..\..\..\bttbl2hfile\Bttbl2Hfile pcieboot_ddrinit.btbl pcieboot_ddrinit.h pcieboot_ddrinit.bin

..\..\..\..\..\..\hfile2array\hfile2array pcieboot_ddrinit.h pcieDdrInit.h ddrInitCode

move pcieDdrInit.h ..\..\..\linux_host_loader\pcieDdrInit_%TARGET%.h


