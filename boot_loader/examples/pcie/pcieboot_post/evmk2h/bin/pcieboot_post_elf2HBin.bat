set C6000_CG_DIR=%C6X_GEN_INSTALL_PATH%
set TOOL_DIR="..\..\..\..\..\..\"
set TARGET=k2h
set ENDIAN=little
set PATH=%PATH%;%SystemRoot%\system32;%SystemRoot%;


@echo off

echo C6000_CG_DIR set as: %C6000_CG_DIR%
echo TARGET set as: %TARGET%
echo IBL_ROOT_DIR set as : %IBL_ROOT_DIR%

copy ..\..\..\..\..\..\post\evm%TARGET%\bin\post_evm%TARGET%.out

echo Converting .out to HEX ...
if %ENDIAN% == little (
%C6000_CG_DIR%\bin\hex6x -order L post.rmd post_evm%TARGET%.out
) else (
%C6000_CG_DIR%\bin\hex6x -order M post.rmd post_evm%TARGET%.out
)

..\..\..\..\..\..\bttbl2hfile\Bttbl2Hfile pcieboot_post.btbl pcieboot_post.h pcieboot_post.bin
..\..\..\..\..\..\hfile2array\hfile2array pcieboot_post.h post.h post
move post.h ..\..\..\linux_host_loader\post_%TARGET%.h
