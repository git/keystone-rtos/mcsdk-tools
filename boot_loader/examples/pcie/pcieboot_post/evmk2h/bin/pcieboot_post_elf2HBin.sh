export C6000_CG_DIR=~/ti/ccsv5/tools/compiler/c6000_7.4.1
export TOOL_DIR="../../../../../../"
export TARGET=k2h
export ENDIAN=little

echo C6000_CG_DIR set as: ${C6000_CG_DIR}
echo TARGET set as: ${TARGET}
echo IBL_ROOT_DIR set as : ${IBL_ROOT_DIR}

cp ../../../../../../post/evm${TARGET}/bin/post_evm${TARGET}.out .

echo Converting .out to HEX ...
if [ ${ENDIAN} == little ]
then
${C6000_CG_DIR}/bin/hex6x -order L post.rmd post_evm${TARGET}.out
else
${C6000_CG_DIR}/bin/hex6x -order M post.rmd post_evm${TARGET}.out
fi

../../../../../../bttbl2hfile/Bttbl2Hfile pcieboot_post.btbl pcieboot_post.h pcieboot_post.bin
../../../../../../hfile2array/hfile2array pcieboot_post.h post.h post
mv post.h ../../../linux_host_loader/post_${TARGET}.h
