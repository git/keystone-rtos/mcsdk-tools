################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/pcieboot_localreset.obj: ../../../../../examples/pcie/pcieboot_localreset/src/pcieboot_localreset.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --define=_EVMC6678L_ --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/pcieboot_localreset.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


