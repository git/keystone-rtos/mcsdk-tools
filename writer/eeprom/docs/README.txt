EEPROM Writer Utility

EEPROM Writer is a simple utility to program a CCS format image/data file to I2C EEPROM.

Steps to program the EEPROM with a CCS format data file (.out or .dat):

1. Be sure to set the boot mode dip switch to no boot/EMIF16 boot mode on the EVM. 

2. Copy the CCS format data file to writer\eeprom\evmxxxx\bin directory, and rename it to app.dat.

3. Change the file_name, bus_addr, start_addr and swap_data in writer\eeprom\evmxxxx\bin\eepromwriter_input.txt if necessary. 
   By default the EEPROM writer will load app.dat to DSP memory and write the data to I2C slave
   bus address 81 (0x51) at EEPROM device start byte address 0 with data swap disabled (0).

4. Open CCSv5 and launch the evmxxxx emulator target configuration and connect to core 0.

5. Load the program writer\eeprom\evmxxxx\bin\eepromwriter_evmxxxx.out to CCS, be sure evmxxxx.gel is used in CCS
   and DDR is intialized.

6. Open the Memory view (in CCSv5, view->Memory Browser), and view the memory address 0x0C000000.

7. Load app.dat to 0x0C000000:
     * In CCSv5, right click mouse in memory window, select "load memory".
     * Browse and select writer\eeprom\evmxxxx\bin\app.dat.
     * Select the option "Use the file header information to set the start address and size of the memory block to be 
       loaded" and click "next".
     * Change the Start Address to "0x0C000000" if it is not 0x0C000000, and click "finish".

8.After the data file is loaded into the memory, run the program (in CCSv5, press F8), it will start to program the 
   EEPROM.

9.When programming is completed, the console will print "EEPROM programming completed successfully", if there
   is any error, the console will show the error message.

Steps to program the EEPROM with a binary file (.bin):

1. Be sure to set the boot mode dip switch to no boot/EMIF16 boot mode on the EVM. 

2. Copy the binary file to writer\eeprom\evmxxxx\bin directory.

3. Change the file_name, bus_addr, start_addr and swap_data in writer\eeprom\evmxxxx\bin\eepromwriter_input.txt if necessary. 
   Make sure that file_name is set to the name of your binary file.
   
  *By default the EEPROM writer will load app.dat to DSP memory and write the data to I2C slave
   bus address 81 (0x51) at EEPROM device start byte address 0 with data swap disabled (0).
   
4. Open CCSv5 and launch the evmxxxx emulator target configuration and connect to core 0.

5. Load the program writer\eeprom\evmxxxx\bin\eepromwriter_evmxxxx.out to CCS, be sure evmxxxx.gel is used in CCS
   and DDR is intialized.

6. Open the Memory view (in CCSv5, view->Memory Browser), and view the memory address 0x0C000000.

7. Load your .bin file from step 3 to 0x0C000000:
     * In CCSv5, right click mouse in memory window, select "load memory".
     * Browse and select your binary file. 
         - By default, the browse menu only displays .dat files. You will have to change the option
           TI Data Format (*.dat) to Raw Data Format (*.bin) to find your binary file.
     * Click "Next".
     * Change the Start Address to "0x0C000000" if it is not 0x0C000000.
     * Change the Type-size to 32 bits if it is not 32 bits.
	 * Make sure the swap checkbox is unchecked.
     * click "Finish".

8.After the data file is loaded into the memory, run the program (in CCSv5, press F8), it will start to program the 
   EEPROM.

9.When programming is completed, the console will print "EEPROM programming completed successfully", if there
   is any error, the console will show the error message.

Steps to re-build eepromwriter:

1. Uses CCS to build eepromwriter:
   * Import the eepromwriter CCS project from writer\eeprom\evmxxxx directory (in CCSv5, Project->Import Existing CCS/
   CCE Eclipse Projects).
   * Clean and build the eepromwriter project.
   * After the project build is completed, eepromwriter_evmxxxx.out and eepromwriter_evmxxxx.map will be generated under 
     writer\eeprom\evmxxxx\bin directory.

2. Uses Makefile to build eepromwriter:
   NOTE FOR BUILDING ON WINDOWS ENVIRONMENT: For building on windows environment GNU utilities like
   "make" would be required. The following build procedure should also work on MINGW-MSYS Bourne shell.

    Before starting the build following environment setup has to be done 
    1) variable C_DIR should be set to the top directory of the Code Generation tools e.g.
       Linux bash shell: 
          export C_DIR=/opt/TI/TI_CGT_C6000_7.2.1/
       MSYS bash shell: 
          export C_DIR='"C:/ti/ccsv5/tools/compiler/c6000"'
    2) Code Generation tool binaries should be in the path e.g.
       Linux bash shell: 
          export PATH=/opt/TI/TI_CGT_C6000_7.2.1/bin:$PATH
       MSYS bash shell: 
          export PATH=$PATH:/c/ti/ccsv5/tools/compiler/c6000/bin/
    3) variable PFORM_LIB_DIR should be set the directory of the Platform Library root, e.g.
       Linux bash shell: 
          export PFORM_LIB_DIR=pdk_xxxx_x_x_x_xx/packages/ti/platform
       MSYS bash shell: 
          export PFORM_LIB_DIR='"C:/ti/pdk_xxxx_x_x_x_xx/packages/ti/platform"'

    The makefile for building the eepromwriter is in the directory "tools/writer/eeprom/evmxxxx"
    Following are the steps to build eepromwriter, e.g.:
    cd tools/writer/eeprom/evmxxxx
    make DEVICE=<device number>
        supported device numbers are 
            C6678
            C6670
            C6657
            TCI6636K2H
			TCI6636K2E
			TCI6630K2L
	    66AK2G02

Please refer to BIOS MCSDK x.0 User's Guide (http://processors.wiki.ti.com/index.php/BIOS_MCSDK_x.0_User_Guide) for more details.