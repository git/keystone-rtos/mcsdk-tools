/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**************************************************************************************
 * FILE PURPOSE: QSPI Flash writer utility
 **************************************************************************************
 * FILE NAME: qspiflashwriter.c
 *
 * DESCRIPTION: A simple flash writer using platform lib APIs to program the
 *              QSPI flash with an image that the ibl can read.
 *
 ***************************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "platform.h"
#include "types.h"

/* QSPI flash writer utility version */
char version[] = "01.00.00.00";

/* The input file name is hard coded */
char *input_file = "qspi_flash_writer_input.txt";

uint32_t swap_byte = 0;

/* Parameters defined in the input_file */
#define FILE_NAME      "file_name"
#define START_ADDR     "start_addr"

/* Memory address to store the write data */
#define WRITE_DATA_ADDRESS     0x80000000
#define QSPI_FLASH_SIZE              (64 * 1024 * 1024)

/******************************************************************************
 * Structure:   QSPI_FLASH_WRITER_INFO_T
 *
 *              QSPI Flash writer control data. This structure should be
 *              filled in by the user before running
 ******************************************************************************/
#define MAX_LINE_LENGTH 40
typedef struct QSPI_FLASH_WRITER_INFO_tag
{
    char        file_name[MAX_LINE_LENGTH]; /* CCS format data file name */
    uint32_t    filesize;                 /* Number of bytes to be written into the device */
    uint32_t    blockSizeBytes;             /* The size of each sector */
    uint32_t    deviceTotalBytes;           /* Total number of bytes available in the device */
    uint32_t    startAddr;                  /* Start address to write */
    uint32_t    writeBytes;                 /* Number of bytes to be written into the device */
    uint8_t     *writeData;                 /* Address to store the write data */
    uint8_t     *readData;                  /* Address to store the read data */

} QSPI_FLASH_WRITER_INFO_T;

QSPI_FLASH_WRITER_INFO_T qspiFlashWriterInfo;

/* OSAL functions for Platform Library */
uint8_t *Osal_platformMalloc (uint32_t num_bytes, uint32_t alignment)
{
	return malloc(num_bytes);
}

void Osal_platformFree (uint8_t *dataPtr, uint32_t num_bytes)
{
    /* Free up the memory */
    if (dataPtr)
    {
        free(dataPtr);
    }
}

void Osal_platformSpiCsEnter(void)
{
    return;
}

void Osal_platformSpiCsExit (void)
{
    return;
}

/******************************************************************************
 * Function:    print_platform_errno
 ******************************************************************************/
void
print_platform_errno
(
    void
)
{
    printf ("Returned platform error number is %d\n", platform_errno);
}

/******************************************************************************
 * Function:    formBlock
 *
 *      Form a block of data to write to the QSPI flash. The block is
 *      created as a byte stream from the 4 byte stream in which
 *      the MSB is always sent first.
 ******************************************************************************/
void
formBlock
(
    uint32_t      *data,
    uint32_t      blockSize,
    uint8_t       *scratch
)
{
    uint32_t i, j;

    /* Convert the data to a byte stream */
    for (i = j = 0; j < blockSize; i++, j+=4)
    {
        scratch[j+0] = (data[i] >> 24) & 0xff;
        scratch[j+1] = (data[i] >> 16) & 0xff;
        scratch[j+2] = (data[i] >>  8) & 0xff;
        scratch[j+3] = (data[i] >>  0) & 0xff;
    }
}

/******************************************************************************
 * Function:    write_qspi_flash
 *
 *              Write the image to flash.
 *              Returns TRUE if the image is written successfully
 *                      FALSE if the image write fails
 ******************************************************************************/
Bool
write_qspi_flash
(
    PLATFORM_DEVICE_info    *p_device
)
{
    uint32_t      wPos, wLen;
    uint32_t      block, start_block;
    uint8_t       *scrach_block;

    if (swap_byte)
    {
        scrach_block = malloc(qspiFlashWriterInfo.blockSizeBytes);
        if (scrach_block == NULL)
        {
            printf ("Can not allocate scratch block memory!\n");
            return (FALSE);
        }
    }

    start_block = qspiFlashWriterInfo.startAddr / qspiFlashWriterInfo.blockSizeBytes;

    for (block = start_block, wPos = 0; wPos < qspiFlashWriterInfo.filesize; block++, wPos += qspiFlashWriterInfo.blockSizeBytes)
    {
        printf ("Erasing sector %d \n", block);
        if (platform_device_erase_block (p_device->handle, block))
        {
               printf ("platform_device_erase_block sector # %d failed!\n", start_block);
               print_platform_errno();
               if (swap_byte) free (scrach_block);
               return (FALSE);
        }
    }
    /* Program the QSPI flash */
    for (block = start_block, wPos = 0; wPos < qspiFlashWriterInfo.writeBytes; block++, wPos += qspiFlashWriterInfo.blockSizeBytes)
    {
        printf ("Flashing sector %d (%d bytes of %d)\n", block, wPos, qspiFlashWriterInfo.writeBytes);

        wLen = qspiFlashWriterInfo.blockSizeBytes;
        if ((qspiFlashWriterInfo.filesize - wPos) < qspiFlashWriterInfo.blockSizeBytes)
        {
            wLen = (qspiFlashWriterInfo.filesize - wPos);
        }

        if (swap_byte)
        {
            formBlock((uint32_t *)(&qspiFlashWriterInfo.writeData[wPos]), wLen, scrach_block);
        }
        else
        {
            scrach_block = &qspiFlashWriterInfo.writeData[wPos];
        }

        if (platform_device_write(p_device->handle,
                                  (block * qspiFlashWriterInfo.blockSizeBytes),
                                  scrach_block,
                                  wLen) != Platform_EOK)
        {
            printf ("platform_device_write sector # %d failed!\n", block);
            print_platform_errno();
            if (swap_byte) free (scrach_block);
            return (FALSE);
        }
    }

    if (swap_byte) free (scrach_block);
    return (TRUE);
}

/******************************************************************************
 * Function:    flash_verify
 *
 *              Read back the data file that was just flashed.
 *              Returns TRUE if the image verified correctly.
 *                      FALSE if the image verification failed
 ******************************************************************************/
Bool
flash_verify
(
    PLATFORM_DEVICE_info    *p_device
)
{
    uint32_t      rPos, rLen;
    uint32_t      i, j;
    uint32_t      block, start_block;
    uint8_t       *scrach_block;
    uint32_t      *read_data_w;

    if (swap_byte)
    {
        scrach_block = malloc(qspiFlashWriterInfo.blockSizeBytes);
        if (scrach_block == NULL)
        {
            printf ("Can not allocate scratch block memory!\n");
            return (FALSE);
        }
    }

    start_block = qspiFlashWriterInfo.startAddr / qspiFlashWriterInfo.blockSizeBytes;

    for (block = start_block, rPos = 0; rPos < qspiFlashWriterInfo.writeBytes; block++, rPos += qspiFlashWriterInfo.blockSizeBytes)
    {
        printf ("Reading and verifying sector %d (%d bytes of %d)\n", block, rPos, qspiFlashWriterInfo.writeBytes);

        if (!swap_byte)
        {
            scrach_block = &qspiFlashWriterInfo.readData[rPos];

        }

        /* Read a sector of data */
        if(platform_device_read(p_device->handle,
                                (block * qspiFlashWriterInfo.blockSizeBytes),
                                scrach_block,
                                qspiFlashWriterInfo.blockSizeBytes) != Platform_EOK)
        {
            printf ("Failure in sector %d\n", block);
            print_platform_errno();
            if (swap_byte) free (scrach_block);
            return (FALSE);
        }

        /* Convert the packed data */
        if (swap_byte)
        {
            read_data_w = (uint32_t *)(&qspiFlashWriterInfo.readData[rPos]);
            for  (i = 0, j = 0; i < qspiFlashWriterInfo.blockSizeBytes; i += 4)
            {
                read_data_w[j++] = (scrach_block[i+0] << 24) |
                                   (scrach_block[i+1] << 16) |
                                   (scrach_block[i+2] << 8)  |
                                    scrach_block[i+3];
			}
        }

        /* Read the data from the file */
        rLen = qspiFlashWriterInfo.blockSizeBytes;
        if ((qspiFlashWriterInfo.writeBytes - rPos) < qspiFlashWriterInfo.blockSizeBytes)
        {
            rLen = (qspiFlashWriterInfo.writeBytes - rPos);
        }

        for (i = rPos; i < rLen; i++)
        {
            if (qspiFlashWriterInfo.readData[i] != qspiFlashWriterInfo.writeData[i])
            {
                printf ("Failure in sector %d, at byte %d, (at byte %d in the data file) expected 0x%08x, read 0x%08x\n",
                        block, i, rPos, qspiFlashWriterInfo.writeData[i], qspiFlashWriterInfo.readData[i]);
                if (swap_byte) free (scrach_block);
                return (FALSE);
            }
        }

    }

    if (swap_byte) free (scrach_block);
    return (TRUE);
}

/******************************************************************************
 * Function:    parse_input_file
 ******************************************************************************/
static Bool
parse_input_file
(
    FILE*               fp
)
{
    char line[MAX_LINE_LENGTH];
    char tokens[] = " :=;\n\r";
    char *key, *data;

    memset(line, 0, MAX_LINE_LENGTH);

    fgets(line, MAX_LINE_LENGTH, fp);
    key  = (char *)strtok(line, tokens);
    data = (char *)strtok(NULL, tokens);

    if(strlen(data) == 0)
    {
       return FALSE;
    }

    if(strcmp(key, FILE_NAME) != 0)
    {
        return FALSE;
    }

    strcpy (qspiFlashWriterInfo.file_name, data);

    fgets(line, MAX_LINE_LENGTH, fp);
    key  = (char *)strtok(line, tokens);
    data = (char *)strtok(NULL, tokens);

    if(strlen(data) == 0)
    {
       return FALSE;
    }

    if(strcmp(key, START_ADDR) != 0)
    {
        return FALSE;
    }

    qspiFlashWriterInfo.startAddr = (uint32_t)atoi(data);

    return TRUE;
}

/******************************************************************************
 * Function:    find_file_length
 ******************************************************************************/
static Bool
find_file_length
(
    FILE*               fp
)
{
    char        line[MAX_LINE_LENGTH];
    char        *pEnd;
    char        *ext;
    uint32_t    data_len, write_addr;

    memset(line, 0, MAX_LINE_LENGTH);

    ext = strrchr(qspiFlashWriterInfo.file_name, '.');


    if (ext && (strcmp(ext, ".dat") == 0))
    {
    fgets(line, MAX_LINE_LENGTH, fp);

    /* Read the write address from the CCS header */
    strtoul (line,&pEnd,16);
    strtoul (pEnd,&pEnd,16);
    write_addr = strtoul (pEnd,&pEnd,16);
    strtoul (pEnd,&pEnd,16);

    /* Read the data length */
    data_len = (strtoul (pEnd,NULL,16)) * 4;
    }
    else
    {
        /* find the data length by seeking to the end and getting position */
        fseek(fp, 0, SEEK_END);
        data_len = ftell(fp);
        fseek(fp, 0, SEEK_SET);
    }
    printf ("The contents of file %s of length %d will be written to QSPI flash of size 0x%x.\n", qspiFlashWriterInfo.file_name, data_len, QSPI_FLASH_SIZE);
    if (data_len > (qspiFlashWriterInfo.deviceTotalBytes - qspiFlashWriterInfo.startAddr))
    {
        printf ("The data file is too big to fit into the device.\n");
        return FALSE;
    }

    qspiFlashWriterInfo.writeBytes = data_len;
    if (write_addr != WRITE_DATA_ADDRESS)
        write_addr = WRITE_DATA_ADDRESS;
    memset((void *)write_addr, 0xFF, qspiFlashWriterInfo.blockSizeBytes);
    qspiFlashWriterInfo.filesize= fread((void *)write_addr, 1, data_len, fp);
    printf ("The contents of file %s read is of length %d.\n", qspiFlashWriterInfo.file_name, qspiFlashWriterInfo.filesize);
    qspiFlashWriterInfo.writeData  = (uint8_t *)write_addr;
    qspiFlashWriterInfo.readData   = (uint8_t *)(write_addr + qspiFlashWriterInfo.deviceTotalBytes);

    return TRUE;
}

/******************************************************************************
 * Function:    main
 ******************************************************************************/
void main ()
{
    FILE                    *fp;
    platform_init_flags     init_flags;
    platform_init_config    init_config;
    PLATFORM_DEVICE_info    *p_device;
    Bool                    ret;

    printf("QSPI Flash Writer Utility Version %s\n\n", version);

    fp = fopen(input_file, "r");
    if (fp == NULL)
    {
        printf("Error in opening %s input file\n", input_file);
        return;
    }

    ret = parse_input_file(fp);
    fclose (fp);

    if (ret == FALSE)
    {
        printf("Error in parsing %s input file\n", input_file);
        return;
    }

    /* Initialize main Platform lib */
    memset(&init_config, 0, sizeof(platform_init_config));
    memset(&init_flags, 1, sizeof(platform_init_flags));
    init_flags.pll = 0;
    init_flags.ddr = 0;
    init_flags.phy = 0;
    if (platform_init(&init_flags, &init_config) != Platform_EOK)
    {
        printf ("Platform init failed!\n");
        print_platform_errno();
        return;
    }

    p_device = platform_device_open(PLATFORM_DEVID_QSPIFLASH_S25FL512S, 0);
    if (p_device == NULL)
    {
        printf ("QSPI Flash device open failed!\n");
        print_platform_errno();
        return;
    }
    qspiFlashWriterInfo.deviceTotalBytes  = p_device->block_count * p_device->page_count * p_device->page_size;
    qspiFlashWriterInfo.blockSizeBytes    = p_device->page_count * p_device->page_size;

    if ((qspiFlashWriterInfo.startAddr % qspiFlashWriterInfo.blockSizeBytes) != 0)
    {
        printf ("The start programming address 0x%8x set in %s is not at the beginning of a block, block size = 0x%4x\n",
                qspiFlashWriterInfo.startAddr,
                qspiFlashWriterInfo.file_name,
                qspiFlashWriterInfo.blockSizeBytes);
        return;
    }

    /* Open and find the length of the data file */
    fp = fopen (qspiFlashWriterInfo.file_name, "rb");
    if (fp == NULL)
    {
      printf ("Failed to open file %s\n", qspiFlashWriterInfo.file_name);
      platform_device_close(p_device->handle);
      return;
    }

    /* Parse the CCS format file */
    ret = find_file_length(fp);
    fclose (fp);

    qspiFlashWriterInfo.writeBytes += qspiFlashWriterInfo.blockSizeBytes - 1;
    qspiFlashWriterInfo.writeBytes -= qspiFlashWriterInfo.writeBytes % qspiFlashWriterInfo.blockSizeBytes;

    if (ret == FALSE)
    {
        printf("Error in parsing CCS file %s\n", qspiFlashWriterInfo.file_name);
        platform_device_close(p_device->handle);
        return;
    }

    /* Write the flash */
    if (write_qspi_flash (p_device) == FALSE)
    {
        printf ("QSPI Flash write failed\n");
        platform_device_close(p_device->handle);
        return;
    }

    /* verify the flash */
    if(flash_verify (p_device) == FALSE)
    {
        printf ("QSPI Flash read verify failed\n");
        platform_device_close(p_device->handle);
        return;
    }


    printf ("QSPI Flash programming completed successfully\n");

    platform_device_close(p_device->handle);

    return;
}

