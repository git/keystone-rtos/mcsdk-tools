################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/qspiflashwriter.obj: ../../../../writer/qspi_flash/src/qspiflashwriter.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/board/src/flash/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="../../../../writer/qspi_flash/include" --include_path="$(PDK_INSTALL_PATH)/ti/board" --define=SOC_K2G --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/qspiflashwriter.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


