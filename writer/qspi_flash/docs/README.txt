QSPI flash Writer Utility

QSPI flash Writer is a simple utility to program a CCS format image/data file to the QSPI flash.

Steps to program the QSPI flash:

1. Be sure to set the boot mode dip switch to no boot/EMIF16 boot mode on the EVM.

2. Copy the binary file to writer\qspi_flash\evmxxxx\bin directory, and rename it to app.bin.

3. Change the file_name and start_addr in writer\qspi_flash\evmxxxx\bin\qspi_flash_writer_input.txt if necessary. 
   By default the QSPI flash writer will load app.bin to DSP memory and write the data to QSPI flash device start byte address 0, 
   the start_addr should always be set to the start byte addess of a sector.

4. Open CCS and launch the evmxxxx emulator target configuration and connect to core 0.

5. Load the program writer\qspi_flash\evmxxxx\bin\qspiflashwriter_evmxxxx.out to CCS, be sure evmxxxx.gel is used in CCS 
   and DDR is intialized.

6. Open the Memory view (in CCS, view->Memory Browser), and view the memory address 0x80000000.

7. Load app.bin to 0x80000000:
     * In CCS, right click mouse in memory window, select "load memory".
     * Browse and select writer\qspi_flash\evmxxxx\bin\app.bin (raw data format), click "next"
     * Set the Start Address to "0x80000000", Type-size to 32-bits, leave swap unchecked, click "finish"


8. After the binary file is loaded into the memory, run the program (in CCS, press F8), it will start to program the 
   QSPI flash.

9. When programming is completed, the console will print "QSPI Flash programming completed successfully", if there
   is any error, the console will show the error message.


Steps to re-build qspiflashwriter:

Uses CCS to build qspiflashwriter:
   * Import the qspiflashwriter CCS project from writer\qspi_flash\evmxxxx directory (in CCS, Project->Import Existing CCS/
   CCE Eclipse Projects).
   * Clean and build the qspiflashwriter project.
   * After the project build is completed, qspiflashwriter_evmxxxx.out and qspiflashwriter_evmxxxx.map will be generated under 
     writer\qspi_flash\evmxxxx\bin directory.

Please refer to MCSDK User's Guide (http://processors.wiki.ti.com/index.php/MCSDK_User_Guide_for_KeyStone_II) for more details.