NAND Writer Utility

NAND Writer is a simple utility to program a CCS format image/data file to the NAND flash.

Steps to program the NAND:

1. Be sure to set the boot mode dip switch to no boot/EMIF16 boot mode on the EVM.

2. Copy the binary file to writer\nand\evmxxxx\bin directory, and rename it to app.bin.

3. Change the file_name and start_addr in writer\nand\evmxxxx\bin\nandwriter_input.txt if necessary. 
   By default the NAND writer will load app.bin to DSP memory and write the data to NAND device start byte address 16384
   (start address of block 1). The start_addr should always be set to the start byte addess of a block.
   
4. Open CCSv5 and launch the evmxxxx emulator target configuration and connect to core 0.

5. Load the program writer\nand\evmxxxx\bin\nandwriter_evmxxxx.out to CCS, be sure evmxxxx.gel is used in CCS 
   and DDR is intialized.

6. Open the Memory view (in CCSv5, view->Memory Browser), and view the memory address 0x80000000.

7. Load app.bin to 0x80000000:
     * In CCSv5, right click mouse in memory window, select "load memory".
     * Browse and select writer\nand\evmxxxx\bin\app.bin (raw data format), click "next"
     * Set the Start Address to "0x80000000", Type-size to 32-bits, leave swap unchecked, click "finish"

8. After the data file is loaded into the memory, run the program (in CCSv5, press F8), it will start to program the 
   NAND.

9. When programming is completed, the console will print "NAND programming completed successfully", if there
   is any error, the console will show the error message.


Steps to re-build nandwriter:

1. Uses CCS to build nandwriter:
   * Import the nandwriter CCS project from writer\nand\evmxxxx directory (in CCSv5, Project->Import Existing CCS/
   CCE Eclipse Projects).
   * Clean and build the nandwriter project.
   * After the project build is completed, nandwriter_evmxxxx.out and nandwriter_evmxxxx.map will be generated under 
     writer\nand\evmxxxx\bin directory.

2. Uses Makefile to build nandwriter:
   NOTE FOR BUILDING ON WINDOWS ENVIRONMENT: For building on windows environment GNU utilities like
   "make" would be required. The following build procedure should also work on MINGW-MSYS Bourne shell.

    Before starting the build following environment setup has to be done 
    1) variable C_DIR should be set to the top directory of the Code Generation tools e.g.
       Linux bash shell: 
          export C_DIR=/opt/TI/TI_CGT_C6000_7.2.1/
       MSYS bash shell: 
          export C_DIR='"C:/ti/ccsv5/tools/compiler/c6000"'
    2) Code Generation tool binaries should be in the path e.g.
       Linux bash shell: 
          export PATH=/opt/TI/TI_CGT_C6000_7.2.1/bin:$PATH
       MSYS bash shell: 
          export PATH=$PATH:/c/ti/ccsv5/tools/compiler/c6000/bin/
    3) variable PFORM_LIB_DIR should be set the directory of the Platform Library root, e.g.
       Linux bash shell: 
          export PFORM_LIB_DIR=pdk_xxxx_x_x_x_xx/packages/ti/platform
       MSYS bash shell: 
          export PFORM_LIB_DIR='"C:/ti/pdk_xxxx_x_x_x_xx/packages/ti/platform"'

    The makefile for building the nandwriter is in the directory "tools/writer/nand/evmxxxx"
    Following are the steps to build nandwriter, e.g.:
    cd tools/writer/nand/evmxxxx
    make DEVICE=<device number>
        supported device numbers are 
            C6678
            C6670
            C6657
            TCI6636K2H
			TCI6636K2E
			TCI6630K2L
	    66AK2G02		

Please refer to BIOS MCSDK x.0 User's Guide (http://processors.wiki.ti.com/index.php/BIOS_MCSDK_x.0_User_Guide) for more details.