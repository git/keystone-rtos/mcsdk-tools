section 
{
  param_index    = 0
  boot_mode      = 40
  sw_pll_prediv	 = 1
  sw_pll_mult    = 16
  sw_pll_postdiv = 2
  options        = 1

  core_freq_mhz    = 625
  i2c_clk_freq_khz = 200

  dev_addr_ext = 0x50

  multi_i2c_id = 0
  my_i2c_id    = 1
  address_delay = 0
  exe_file = "post.i2c.ccs"
}






