/**
 *   @file  cpmac_regs.h
 *
 *   @brief   
 *      This file contains the Register Desciptions for EMAC
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2008, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#ifndef __CPMAC_REGS_H__
#define __CPMAC_REGS_H__

/**
 * @brief 
 *  The structure describes the CPMAC Register Overlay.
 *
 * @details
 *  This is the CPMAC Register overlay data structure which is used
 *  by the CPMAC Driver.
 */
typedef struct CPMAC_REGS
{
    volatile uint32_t TXIDVER;
    volatile uint32_t TXCONTROL;
    volatile uint32_t TXTEARDOWN;
    volatile uint8_t  RSVD0[4];
    volatile uint32_t RXIDVER;
    volatile uint32_t RXCONTROL;
    volatile uint32_t RXTEARDOWN;
    volatile uint8_t  RSVD1[100];
    volatile uint32_t TXINTSTATRAW;
    volatile uint32_t TXINTSTATMASKED;
    volatile uint32_t TXINTMASKSET;
    volatile uint32_t TXINTMASKCLEAR;
    volatile uint32_t MACINVECTOR;
    volatile uint8_t  RSVD2[12];
    volatile uint32_t RXINTSTATRAW;
    volatile uint32_t RXINTSTATMASKED;
    volatile uint32_t RXINTMASKSET;
    volatile uint32_t RXINTMASKCLEAR;
    volatile uint32_t MACINTSTATRAW;
    volatile uint32_t MACINTSTATMASKED;
    volatile uint32_t MACINTMASKSET;
    volatile uint32_t MACINTMASKCLEAR;
    volatile uint8_t  RSVD3[64];
    volatile uint32_t RXMBPENABLE;
    volatile uint32_t RXUNICASTSET;
    volatile uint32_t RXUNICASTCLEAR;
    volatile uint32_t RXMAXLEN;
    volatile uint32_t RXBUFFEROFFSET;
    volatile uint32_t RXFILTERLOWTHRESH;
    volatile uint8_t  RSVD4[8];
    volatile uint32_t RX0FLOWTHRESH;
    volatile uint32_t RX1FLOWTHRESH;
    volatile uint32_t RX2FLOWTHRESH;
    volatile uint32_t RX3FLOWTHRESH;
    volatile uint32_t RX4FLOWTHRESH;
    volatile uint32_t RX5FLOWTHRESH;
    volatile uint32_t RX6FLOWTHRESH;
    volatile uint32_t RX7FLOWTHRESH;
    volatile uint32_t RX0FREEBUFFER;
    volatile uint32_t RX1FREEBUFFER;
    volatile uint32_t RX2FREEBUFFER;
    volatile uint32_t RX3FREEBUFFER;
    volatile uint32_t RX4FREEBUFFER;
    volatile uint32_t RX5FREEBUFFER;
    volatile uint32_t RX6FREEBUFFER;
    volatile uint32_t RX7FREEBUFFER;
    volatile uint32_t MACCONTROL;
    volatile uint32_t MACSTATUS;
    volatile uint32_t EMCONTROL;
    volatile uint32_t FIFOCONTROL;
    volatile uint32_t MACCONFIG;
    volatile uint32_t SOFTRESET;
    volatile uint8_t  RSVD5[88];
    volatile uint32_t MACSRCADDRLO;
    volatile uint32_t MACSRCADDRHI;
    volatile uint32_t MACHASH1;
    volatile uint32_t MACHASH2;
    volatile uint32_t BOFFTEST;
    volatile uint32_t TPACETEST;
    volatile uint32_t RXPAUSE;
    volatile uint32_t TXPAUSE;
    volatile uint8_t  RSVD6[16];
    volatile uint32_t RXGOODFRAMES;
    volatile uint32_t RXBCASTFRAMES;
    volatile uint32_t RXMCASTFRAMES;
    volatile uint32_t RXPAUSEFRAMES;
    volatile uint32_t RXCRCERRORS;
    volatile uint32_t RXALIGNCODEERRORS;
    volatile uint32_t RXOVERSIZED;
    volatile uint32_t RXJABBER;
    volatile uint32_t RXUNDERSIZED;
    volatile uint32_t RXFRAGMENTS;
    volatile uint32_t RXFILTERED;
    volatile uint32_t RXQOSFILTERED;
    volatile uint32_t RXOCTETS;
    volatile uint32_t TXGOODFRAMES;
    volatile uint32_t TXBCASTFRAMES;
    volatile uint32_t TXMCASTFRAMES;
    volatile uint32_t TXPAUSEFRAMES;
    volatile uint32_t TXDEFERRED;
    volatile uint32_t TXCOLLISION;
    volatile uint32_t TXSINGLECOLL;
    volatile uint32_t TXMULTICOLL;
    volatile uint32_t TXEXCESSIVECOLL;
    volatile uint32_t TXLATECOLL;
    volatile uint32_t TXUNDERRUN;
    volatile uint32_t TXCARRIERSENSE;
    volatile uint32_t TXOCTETS;
    volatile uint32_t FRAME64;
    volatile uint32_t FRAME65T127;
    volatile uint32_t FRAME128T255;
    volatile uint32_t FRAME256T511;
    volatile uint32_t FRAME512T1023;
    volatile uint32_t FRAME1024TUP;
    volatile uint32_t NETOCTETS;
    volatile uint32_t RXSOFOVERRUNS;
    volatile uint32_t RXMOFOVERRUNS;
    volatile uint32_t RXDMAOVERRUNS;
    volatile uint8_t  RSVD7[624];
    volatile uint32_t MACADDRLO;
    volatile uint32_t MACADDRHI;
    volatile uint32_t MACINDEX;
    volatile uint8_t  RSVD8[244];
    volatile uint32_t TX0HDP;
    volatile uint32_t TX1HDP;
    volatile uint32_t TX2HDP;
    volatile uint32_t TX3HDP;
    volatile uint32_t TX4HDP;
    volatile uint32_t TX5HDP;
    volatile uint32_t TX6HDP;
    volatile uint32_t TX7HDP;
    volatile uint32_t RX0HDP;
    volatile uint32_t RX1HDP;
    volatile uint32_t RX2HDP;
    volatile uint32_t RX3HDP;
    volatile uint32_t RX4HDP;
    volatile uint32_t RX5HDP;
    volatile uint32_t RX6HDP;
    volatile uint32_t RX7HDP;
    volatile uint32_t TX0CP;
    volatile uint32_t TX1CP;
    volatile uint32_t TX2CP;
    volatile uint32_t TX3CP;
    volatile uint32_t TX4CP;
    volatile uint32_t TX5CP;
    volatile uint32_t TX6CP;
    volatile uint32_t TX7CP;
    volatile uint32_t RX0CP;
    volatile uint32_t RX1CP;
    volatile uint32_t RX2CP;
    volatile uint32_t RX3CP;
    volatile uint32_t RX4CP;
    volatile uint32_t RX5CP;
    volatile uint32_t RX6CP;
    volatile uint32_t RX7CP;
}CPMAC_REGS;

/* MACADDRLO: Bit Mask Definitions. */
#define CPMAC_MACADDRLO_VALID           (0x00100000u)
#define CPMAC_MACADDRLO_MATCHFILT       (0x00080000u)

/* MACCONTROL: Bit Mask Definitions. */
#define CPMAC_MACCONTROL_RXOWNERSHIP    (0x00002000u)
#define CPMAC_MACCONTROL_RXOFFLENBLOCK  (0x00004000u)
#define CPMAC_MACCONTROL_MIIEN          (0x00000020u)

/* RXMBP Enable: Bit Mask Definitions.  */
#define CPMAC_RXMBPENABLE_RXBROADEN     (0x00002000u)

#endif /* __CPMAC_REGS_H__ */
