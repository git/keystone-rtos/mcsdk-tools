#ifndef _CPDMA_API_H
#define _CPDMA_API_H
/********************************************************************************************************
 * FILE PURPOSE: Provide the cpdma API
 ********************************************************************************************************
 * FILE NAME: cpdma_api.h
 *
 * DESCRIPTION: The public API is defined
 *
 ********************************************************************************************************/


typedef struct cpdmaRxCfg_s  {

    uint32_t  rxBase;             /* Base address of rx registers */
    uint32_t  nRxChans;           /* The number of rx channels */
    uint32_t  flowBase;           /* Add address of flow registers */
    uint32_t  nRxFlows;           /* Number of rx flows */
    uint32_t  qmNumFreeBuf;       /* Queue manager for descriptors/buffers for received packets */
    uint32_t  queueFreeBuf;       /* Queue that holds descriptors/buffers for received packets */
    uint32_t  qmNumRx;            /* Queue manager for received packets */
    uint32_t  queueRx;            /* Default Rx queue for received packets */
    uint32_t  tdownPollCount;     /* Number of loop iterations to wait for teardown */
    
} cpdmaRxCfg_t; 


typedef struct cpdmaTxCfg_s  {

    uint32_t gblCtlBase;          /* Base address of global control registers */
    uint32_t txBase;              /* Base address of the tx registers */
    uint32_t nTxChans;            /* The number of tx channels */
    
} cpdmaTxCfg_t;
    

/* Prototypes */
int16_t hwCpdmaRxDisable (const cpdmaRxCfg_t *cfg);
int16_t hwCpdmaRxConfig (const cpdmaRxCfg_t *cfg);
int16_t hwCpdmaTxConfig (const cpdmaTxCfg_t *cfg);
int16_t hwCpdmaTxDisable (const cpdmaTxCfg_t *cfg);

#endif /* _CPDMA_API_H */
