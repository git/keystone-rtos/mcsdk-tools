/*
 *
 * Copyright (C) 2011-12 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _EVMC6657_PHY_H_
#define _EVMC6657_PHY_H_

#include <stdint.h>

enum PHY_ERR {
	PHY_OK = 0,
	EPHY_FAIL,
	EPHY_INVALID_PARM,
	EPHY_WR_FAILED,
	EPHY_RD_FAILED,
	ENO_SPEED
};

/* operation */
#define LOOPBACK_MASK			0x1F
#define MAC_LOOPBACK 			(1 << 0)
#define LINE_COPPER_LOOPBACK	        (1 << 1)	/* Not Available */
#define LINE_FIBER_LOOPBACK		(1 << 2)	/* Not Available */
#define EXT_COPPER_LOOPBACK		(1 << 3)
#define EXT_FIBER_LOOPBACK		(1 << 4)

/* negotiation (copper) */
#define PHY_SPEED_MASK			(0xF << 5)
#define PHY_MODE_1000MBPS		(1 << 5)
#define PHY_MODE_100MBPS		(1 << 6)
#define PHY_MODE_10MBPS			(1 << 7)
#define PHY_MODE_AUTO			(1 << 8)

/* Preferred Media (copper default) */
#define PHY_PREF_MASK			(7 << 9)
#define PHY_PREF_COPPER			(1 << 9)
#define PHY_PREF_FIBER 			(1 << 10)
#define PHY_PREF_AUTO			(1 << 11)

/* media select */
#define PHY_MEDIA_MASK			(7 << 12)
#define PHY_MEDIA_MAC			(1 << 12)
#define PHY_MEDIA_COPPER		(1 << 13)
#define PHY_MEDIA_FIBER			(1 << 14)

#define PHY_CALC_MASK(fieldOffset, fieldLen, mask)	\
		if ((fieldLen + fieldOffset) >= 16)			\
			mask = (0 - (1 << fieldOffset));		\
		else										\
			mask = (((1 << (fieldLen + fieldOffset))) - (1 << fieldOffset))

int phy_init(int phyaddr, uint32_t mode);

#endif /* _EVMC6657_PHY_H_ */
