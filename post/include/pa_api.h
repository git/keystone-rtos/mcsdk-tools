#ifndef _PA_API_H
#define _PA_API_H
/*****************************************************************************************************
 * FILE PURPOSE: Define the Packet Accelerator API 
 *****************************************************************************************************
 * FILE NAME: pa_api.h
 *
 * DESCRIPTION: The boot loader driver API to the packet accelerator is defined
 *
 *****************************************************************************************************/
 
typedef struct paConfig_s  {

    uint32_t  mac0ms;     /* 32 most significant bits of the mac address */
    uint32_t  mac0ls;     /* 32 least significant bits of the mac address, in the 16msbs of this word */
    uint32_t  mac1ms;     /* 32 most significant bits of the mac address */
    uint32_t  mac1ls;     /* 32 least significant bits of the mac address, in the 16 msbs of this word */
    uint32_t  rxQnum;     /* Receive packet queue number */
    uint8_t   *cmdBuf;    /* Buffer used to create PA command */

} paConfig_t;

/* API */
int16_t hwPaEnable (const paConfig_t *cfg);
int16_t hwPaDisable (void);

#include "hwpafwsw.h"



#endif /* _PA_API_H */

