/******************************************************************************
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/
 
/******************************************************************************
 *
 * File Name:  	evmc665x_emac.h
 *
 * Description:	This file contains typdedefs and macros for EMACSS.
 *
 * History:
 *		FEB/15/2012, Ajay Bhargav		File created
 *
 ******************************************************************************/
#ifndef _EVMC665x_EMAC_H_
#define _EVMC665x_EMAC_H_

/****************
 * Include Files
 ****************/
#include <ti/csl/tistdtypes.h>
#ifdef _EVMC6657L_
#include <ti/csl/cslr_emac.h>
#include <ti/csl/csl_emac.h>


typedef struct EMAC_MCB
{
	EMAC_Desc *rx_desc;
	EMAC_Desc *tx_desc;
	Uint32 lastrxhdp;
} EMAC_MCB;

#endif
/************************
 * Defines and Macros
 ************************/
#ifndef CSL_EMAC_0_REGS
#define CSL_EMAC_0_REGS			(CSL_EMAC_SS_CFG_REGS)
#endif

#ifndef CSL_MDIO_0_REGS1
#define CSL_MDIO_0_REGS			(CSL_EMAC_SS_CFG_REGS + 0x0800)
#endif

#ifndef CSL_SGMII_0_REGS
#define CSL_SGMII_0_REGS		(CSL_EMAC_SS_CFG_REGS + 0x0900)
#endif

#define EMAC_RAM_BASE			(CSL_EMAC_SS_CFG_REGS + 0x2000)
#define EMAC_RAM_LEN      		(0x00002000u)

/* MTU Size */
#define MAX_MTU_SIZE	1536

/* TX & RX Descriptor setup */
#define DESC_SIZE		sizeof(EMAC_Desc)
#define TX_DESC_START	EMAC_RAM_BASE
#define TX_DESC_COUNT	1
#define TX_DESC_ADD(x)	(TX_DESC_START + (DESC_SIZE * x))
#define TX_DESC_END		(TX_DESC_START + (DESC_SIZE * TX_DESC_COUNT))
#define RX_DESC_START	TX_DESC_END
#define RX_DESC_COUNT	1
#define RX_DESC_ADD(x)	(RX_DESC_START + (DESC_SIZE * x))
#define RX_DESC_END		(RX_DESC_START + (DESC_SIZE * RX_DESC_COUNT))
#define RX_BUFF_SIZE	MAX_MTU_SIZE
#define RX_BUFF_START	RX_DESC_END
#define RX_BUFF_ADD(x)	(RX_BUFF_START + (RX_BUFF_SIZE * x))
#define RX_BUFF_END		(RX_BUFF_START + (RX_BUFF_SIZE * RX_DESC_COUNT))
#define TX_BUFF_ADD		RX_BUFF_END

Uint32 EMAC_init();
Uint32 sgmii_init();
Int32 EMAC_Send(Uint8 *buff, int len);
Int32 EMAC_Recv(Uint8 *buff);

#endif /* _EVMC665x_EMAC_H_ */
