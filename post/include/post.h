/******************************************************************************
 * Copyright (c) 2011 Texas Instruments Incorporated - http://www.ti.com
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

/******************************************************************************
 * POST version definitions
 ******************************************************************************/
#define POST_EVM_VERSION_MSG        " POST Version "
#define POST_VERSION                "01.00.00.08"

/******************************************************************************
 * PLL Controller Reset Type Status register
 *
 * Bit 31-29    28   27-16  15-8     7-3      2         1     0
 *     Rsvd  EMU-RST Rsvd  WDRST[N]  Rsvd PLLCTRLRST /RESET  POR
 ******************************************************************************/
#define PLL_BASE                    0x02310000
#define PLL_CTRL_REG_RSTYPE         *( volatile uint32_t* )( PLL_BASE + 0xe4 )

/******************************************************************************
 * UART definitions
 ******************************************************************************/
#define POST_UART_BAUDRATE         115200
#define POST_UART_READ_TIMEOUT     (30 * 1000000) /* in usec */

/******************************************************************************
 * FPGA debug LED definitions
 ******************************************************************************/
typedef enum
{
    POST_LED_OFF     = 0,       /* LED is steady off */
    POST_LED_ON,                /* LED is steady on */
    POST_LED_BLINK              /* LED is blinking */
} POST_LED_STATE;

typedef enum
{
    POST_TEST_RESULT_STARTED = 0,   /* POST Test Result Started */
    POST_TEST_RESULT_PASSED,        /* POST Test Result Passed */
    POST_TEST_RESULT_FAILED         /* POST Test Result Failed */
} POST_TEST_RESULT;

typedef enum
{
    POST_TEST_IN_PROGRESS = 0,      /* POST running in progress */
    POST_TEST_COMPLETE,             /* POST done successfully */
    POST_TEST_DDR,                  /* POST external memory test */
    POST_TEST_EEPROM,               /* POST I2C EEPROM read test */
    POST_TEST_NAND,                 /* POST EMIF16 NAND read test */
    POST_TEST_NOR,                  /* POST SPI NOR read test */
    POST_TEST_UART,                 /* POST UART write test */
    POST_TEST_EMAC,                 /* POST EMAC loopback test */
    POST_TEST_PLL_INIT,             /* POST PLL initialization */
    POST_TEST_NAND_INIT,            /* POST NAND initialization */
    POST_TEST_NOR_INIT,             /* POST NOR initialization  */
    POST_TEST_GENERAL,              /* POST test general */
    POST_TEST_EMAC_LOOPBACK,        /* POST test PA Loopback */
    POST_MAX_TEST_NUM               /* Maximum number of POST LED tests */
} POST_TEST_ID;

#define POST_MAX_NUM_LED        4   /* Total number of LEDs on the EVM */
#if (defined(SOC_K2K) || defined(SOC_K2H) || defined(SOC_K2E) || defined(SOC_K2L))
static uint8_t post_led_status[POST_MAX_TEST_NUM][POST_MAX_NUM_LED] =
{
	/* LED0: red   LED0: green   LED1: blue   LED2: blue */
    {POST_LED_OFF, POST_LED_OFF, POST_LED_ON, POST_LED_ON},        /* POST running in progress */
    {POST_LED_OFF, POST_LED_ON, POST_LED_OFF, POST_LED_OFF},       /* POST done successfully */
    {POST_LED_ON, POST_LED_OFF, POST_LED_OFF, POST_LED_OFF},       /* POST external memory test failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_ON, POST_LED_OFF},        /* POST I2C EEPROM read test failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_OFF, POST_LED_ON},        /* POST EMIF16 NAND read test failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_ON, POST_LED_ON},         /* POST SPI NOR read test failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_BLINK, POST_LED_OFF},     /* POST UART write test failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_OFF, POST_LED_BLINK},     /* POST EMAC loopback test failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_BLINK, POST_LED_ON},      /* POST PLL initialization failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_ON, POST_LED_BLINK},      /* POST NAND initialization failed */
    {POST_LED_ON, POST_LED_OFF, POST_LED_BLINK, POST_LED_BLINK},   /* POST NOR initialization failed */
    {POST_LED_BLINK, POST_LED_OFF, POST_LED_OFF, POST_LED_OFF},    /* POST general failure */
    {POST_LED_BLINK, POST_LED_OFF, POST_LED_ON, POST_LED_OFF}      /* POST PA loopback failure */
};
#else
static uint8_t post_led_status[POST_MAX_TEST_NUM][POST_MAX_NUM_LED] =
{
    {POST_LED_ON, POST_LED_ON, POST_LED_ON, POST_LED_ON},           /* POST running in progress */
    {POST_LED_OFF, POST_LED_OFF, POST_LED_OFF, POST_LED_OFF},       /* POST done successfully */
    {POST_LED_BLINK, POST_LED_OFF, POST_LED_OFF, POST_LED_OFF},     /* POST external memory test failed */
    {POST_LED_OFF, POST_LED_BLINK, POST_LED_OFF, POST_LED_OFF},     /* POST I2C EEPROM read test failed */
    {POST_LED_OFF, POST_LED_OFF, POST_LED_BLINK, POST_LED_OFF},     /* POST EMIF16 NAND read test failed */
    {POST_LED_OFF, POST_LED_OFF, POST_LED_OFF, POST_LED_BLINK},     /* POST SPI NOR read test failed */
    {POST_LED_BLINK, POST_LED_BLINK, POST_LED_OFF, POST_LED_OFF},   /* POST UART write test failed */
    {POST_LED_OFF, POST_LED_BLINK, POST_LED_BLINK, POST_LED_OFF},   /* POST EMAC loopback test failed */
    {POST_LED_OFF, POST_LED_OFF, POST_LED_BLINK, POST_LED_BLINK},   /* POST PLL initialization failed */
    {POST_LED_BLINK, POST_LED_BLINK, POST_LED_BLINK, POST_LED_OFF}, /* POST NAND initialization failed */
    {POST_LED_OFF, POST_LED_BLINK, POST_LED_BLINK, POST_LED_BLINK}, /* POST NOR initialization failed */
    {POST_LED_BLINK, POST_LED_BLINK, POST_LED_BLINK, POST_LED_BLINK}, /* POST general failure */
    {POST_LED_ON, POST_LED_BLINK, POST_LED_BLINK, POST_LED_BLINK}   /* POST PA loopback failure */
};
#endif

#define POST_STATUS_MAX_NUM_CHAR            25      /* Maximum char length of the POST status string */
static char post_status[POST_MAX_TEST_NUM][POST_STATUS_MAX_NUM_CHAR] =
{
    "running in progress ...",
    "done successfully!",
    "external memory",
    "I2C EEPROM read",
#if (!defined(_EVMC6670L_))
    "EMIF16 NAND read",
#else
    "GPIO NAND read",
#endif
    "SPI NOR read",
    "UART write",
    "EMAC loopback",
    "PLL initialization",
    "NAND initialization",
    "NOR initialization",
    "general ",
    "EMAC loopback ",
};

#define POST_LED_BLINK_DELAY    500000  /* 500,000 usec blinking delay */

/******************************************************************************
 * I2C EEPROM test definitions
 ******************************************************************************/
#define POST_EEPROM_TEST_DEVICE_ID      PLATFORM_DEVID_EEPROM50     /* I2C slave bus address 0x50 */
#define POST_EEPROM_TEST_READ_ADDRESS   0                           /* Byte address */
#define POST_EEPROM_TEST_READ_LENGTH    12                          /* Read length in Bytes */

/******************************************************************************
 * NAND test definitions
 ******************************************************************************/
#if (defined(_EVMC6657L_) || defined(SOC_K2K) || defined(SOC_K2H))
	#define POST_NAND_TEST_DEVICE_ID        PLATFORM_DEVID_MT29F1G08ABCHC /* NAND device ID */
	#define POST_NAND_TEST_READ_BLOCK_NUM   0       /* NAND read block number */
	#define POST_NAND_TEST_READ_PAGE_NUM    0       /* NAND read page number */
	#define POST_NAND_TEST_READ_LENGTH      2048     /* Read length in bytes (one page) */
#elif (defined(SOC_K2E))
	#define POST_NAND_TEST_DEVICE_ID        PLATFORM_DEVID_MT29F4G08ABADA /* NAND device ID */
	#define POST_NAND_TEST_READ_BLOCK_NUM   0       /* NAND read block number */
	#define POST_NAND_TEST_READ_PAGE_NUM    0       /* NAND read page number */
	#define POST_NAND_TEST_READ_LENGTH      2048     /* Read length in bytes (one page) */
#elif (defined(SOC_K2L))
	#define POST_NAND_TEST_DEVICE_ID        PLATFORM_DEVID_MT29F16G08ADBCAH4C /* NAND device ID */
	#define POST_NAND_TEST_READ_BLOCK_NUM   0       /* NAND read block number */
	#define POST_NAND_TEST_READ_PAGE_NUM    0       /* NAND read page number */
	#define POST_NAND_TEST_READ_LENGTH      4096     /* Read length in bytes (one page) */
#else
	#define POST_NAND_TEST_DEVICE_ID		PLATFORM_DEVID_NAND512R3A2D
	#define POST_NAND_TEST_READ_BLOCK_NUM   0       /* NAND read block number */
	#define POST_NAND_TEST_READ_PAGE_NUM    0       /* NAND read page number */
	#define POST_NAND_TEST_READ_LENGTH      512     /* Read length in bytes (one page) */
#endif
/******************************************************************************
 * NOR test definitions
 ******************************************************************************/
#if (defined(_EVMC6657L_))
	#define POST_NOR_TEST_DEVICE_ID         PLATFORM_DEVID_NORN25Q032A   /* NOR device ID */
	#define POST_NOR_TEST_READ_ADDR         0       /* Byte address of 0 */
	#define POST_NOR_TEST_READ_LENGTH       256     /* Read length in bytes (one page) */
#else
	#define POST_NOR_TEST_DEVICE_ID         PLATFORM_DEVID_NORN25Q128   /* NOR device ID */
	#define POST_NOR_TEST_READ_ADDR         0       /* Byte address of 0 */
	#define POST_NOR_TEST_READ_LENGTH       256     /* Read length in bytes (one page) */
#endif
/******************************************************************************
 * EMAC test definitions
 ******************************************************************************/
#define POST_EMAC_TEST_PKT_LENGTH   256     /* Ethernet packet payload size in bytes */


/******************************************************************************
 * Serial Number definitions
 ******************************************************************************/
#define POST_MAX_SN_SIZE       10          /* Maximum number of the chars of Serial Number for the EVM */
#define POST_SERIAL_NUM_ADDR   (65536-128) /* Last 128 bytes of EEPROM 0x50 stores the S/N */

extern POST_TEST_RESULT
post_test_emac_loopback
(
    void
);
