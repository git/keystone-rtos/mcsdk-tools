Power On Self Test Utility

POST is a test program that can be programmed to I2C EEPROM and boot directly from I2C bus address 0x50 after POR.
It performs board specific tests, such as external memory test, NAND/NOR/EEPROM read test, LED test, etc.

Steps to build POST:

1. Import the POST CCS project from tools\post\evmxxxx directory. (in CCSv5, Project->Import Existing CCS/
   CCE Eclipse Projects)

2. Be sure the lite version of the Platform Lib is pre-built: 
   pdk_xxxx_x_x_x_x\packages\ti\platform\evmxxxx\platform_lib\lib\debug\ti.platform.evmxxxx.lite.lib
   If the lib is not pre-built, refer to Platform Lib User Guide under pdk_xxxx_x_x_x_x\packages\ti\platform\docs 
   on how to build the platform library.

   Note that the lite version of platform lib only contains the functions that are required by the POST, so that 
   POST can be fit into the EEPROM.

3. Clean the POST project and re-build the project, after build is completed, post_evmxxxx.out and post_evmxxxx.map
   will be generated under tools\post\evmxxxx\bin directory.


Steps to run POST in CCSv5:

1. Be sure to set the boot mode dip switch to no boot/EMIF16 boot mode on the EVM

2. Load the program tools\post\evmxxxx\bin\post_evmxxxx.out to CCS.

3. Connect the 3-pin RS-232 cable from the EVM to the serial port of the PC, and start Hyper Terminal.

4. Create a new connection with the Baud rate set to 115200 bps, Data bits 8, Parity none, Stop bits 1 and 
   Flow control none. Be sure the COM port # is set correctly.

5. Run the program in CCS, POST will send the test status and result to the Hyper Terminal.


Steps to program POST to EEPROM:

1. Copy tools\post\evmxxxx\bin\post_i2crom.bin to tools\writer\eeprom\evmxxxx\bin directory

2. Navigate to tools\writer\eeprom\evmxxxx\bin\ and open eepromwriter_input.txt. Make the following changes:
      - Set the file_name to post_i2crom.bin
      - Set the bus_addr to 0x50
      - Set start_addr to 0
      - Set swap_data to 0
   Save and close eepromwriter_input.txt

3. Refer to tools\writer\eeprom\evmxxxx\docs\README.txt on how to program the raw binary file to EEPROM

4. Once the programming is completed successfully, user can set the boot mode to I2C mode with bus address 0x50 and
   boot the POST directly from the EEPROM after POR. The boot status and test result can be monitored using the Hyper
   Terminal as mentioned in "Steps to run POST in CCSv5".

Please refer to C6678L/C6670L/C6657L EVM boot mode dip switch settings:
http://processors.wiki.ti.com/index.php/TMDXEVM6678L_EVM_Hardware_Setup#Boot_Mode_Dip_Switch_Settings
http://processors.wiki.ti.com/index.php/TMDXEVM6670L_EVM_Hardware_Setup#Boot_Mode_Dip_Switch_Settings
http://processors.wiki.ti.com/index.php/TMDSEVM6657L_EVM_Hardware_Setup#Boot_Mode_Dip_Switch_Settings

and User's Guide for more details:
http://processors.wiki.ti.com/index.php/BIOS_MCSDK_2.0_User_Guide 


Please refer to TCI6636K2H EVM boot mode dip switch settings:
http://processors.wiki.ti.com/index.php/EVMK2H_Hardware_Setup

and User's Guide for more details:
http://processors.wiki.ti.com/index.php/MCSDK_User_Guide_for_KeyStone_II

