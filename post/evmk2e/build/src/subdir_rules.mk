################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/c6678.obj: ../../../post/src/c6678.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)/" --include_path="../../../post/include" --define=SOC_K2E --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/c6678.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cpdma.obj: ../../../post/src/cpdma.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)/" --include_path="../../../post/include" --define=SOC_K2E --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/cpdma.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/gmacsl.obj: ../../../post/src/gmacsl.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)/" --include_path="../../../post/include" --define=SOC_K2E --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/gmacsl.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/pa.obj: ../../../post/src/pa.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)/" --include_path="../../../post/include" --define=SOC_K2E --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/pa.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/post.obj: ../../../post/src/post.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)/" --include_path="../../../post/include" --define=SOC_K2E --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/post.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/psc.obj: ../../../post/src/psc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)/" --include_path="../../../post/include" --define=SOC_K2E --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/psc.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/qm.obj: ../../../post/src/qm.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="$(PDK_INSTALL_PATH)/" --include_path="../../../post/include" --define=SOC_K2E --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/qm.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


