section 
{
  param_index    = 0
  boot_mode      = 40
  sw_pll_prediv	 = 0
  sw_pll_mult    = 0
  sw_pll_postdiv = 0
  options        = 1

  core_freq_mhz    = 100
  i2c_clk_freq_khz = 200

  dev_addr_ext = 0x50

  multi_i2c_id = 0
  my_i2c_id    = 1
  address_delay = 0
  exe_file = "post.i2c.ccs"
}






