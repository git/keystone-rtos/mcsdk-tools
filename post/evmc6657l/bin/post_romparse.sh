export CGT_INSTALL_DIR=${C6X_GEN_INSTALL_PATH}
export TARGET=6657
export ENDIAN=little
export IBL_ROOT_DIR=${PDK_INSTALL_PATH}/ti/boot/ibl

echo CGT_INSTALL_DIR set as: ${CGT_INSTALL_DIR}
echo TARGET set as: ${TARGET}
echo IBL_ROOT_DIR set as : ${IBL_ROOT_DIR}

echo Converting .out to HEX ...
if [ ${ENDIAN} == little ]
then
${CGT_INSTALL_DIR}/bin/hex6x -order L post_image.rmd post_evm${TARGET}l.out
else
${CGT_INSTALL_DIR}/bin/hex6x -order M post_image.rmd post_evm${TARGET}l.out
fi

${IBL_ROOT_DIR}/src/util/btoccs/b2ccs post.b post.ccs

if [ ${ENDIAN} == little ]
then
${CGT_INSTALL_DIR}/bin/hex6x -order L post.rmd post_evm${TARGET}l.out
else
${CGT_INSTALL_DIR}/bin/hex6x -order M post.rmd post_evm${TARGET}l.out
fi

if [ ${ENDIAN} == little ]
then
${IBL_ROOT_DIR}/src/util/bconvert/bconvert64x -le post2.b post.b
else
${IBL_ROOT_DIR}/src/util/bconvert/bconvert64x -be post2.b post.b
fi

${IBL_ROOT_DIR}/src/util/btoccs/b2i2c post.b post.i2c.b

${IBL_ROOT_DIR}/src/util/btoccs/b2ccs post.i2c.b post.i2c.ccs

echo Generating I2C ROM data ...
${IBL_ROOT_DIR}/src/util/romparse/romparse -rom_base 0x50 post_i2crom.map.pp

${IBL_ROOT_DIR}/src/util/btoccs/ccs2bin -swap i2crom.ccs  post_i2crom.bin

rm -f *.ccs *.b
