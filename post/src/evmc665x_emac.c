/******************************************************************************
 * Copyright (c) 2011-2012 Texas Instruments Incorporated - http://www.ti.com
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/

/******************************************************************************
 *
 * File Name:  	evm665x_emac.c
 *
 * Description:	This file contains the EMAC initialization API
 * 
 ******************************************************************************/

/************************
 * Include Files
 ************************/
#ifdef _EVMC6657L_
#include <stdio.h>
#include <stdint.h>
#include <evmc665x_emac.h>
#include <cslr_psc.h>
#include <csl_bootcfgAux.h>
#include <csl_pscAux.h>
#include <string.h>


EMAC_MCB emac_mcb;
volatile Uint8 rxbuff[RX_BUFF_SIZE];


Uint32 EMAC_init()
{
	volatile Uint32* pRegAddr;
	uint32_t i = 0;
	uint32_t mac_addr2, mac_addr1;
#if 0
    uint32_t     power_domain_num          = 0;
    uint32_t     mdctl_emac_module_num     = 3;
    uint32_t     mdstat_emac_module_num    = 3;
    CSL_PSC_MODSTATE mdstat;

	/* EMAC clock domain enable */
	CSL_PSC_enablePowerDomain(power_domain_num);
	CSL_PSC_setModuleNextState (mdctl_emac_module_num, PSC_MODSTATE_ENABLE);

	/* start the process and wait. but timeout in 1000 loops. */
	CSL_PSC_startStateTransition(power_domain_num);
	while(((CSL_PSC_isStateTransitionDone (power_domain_num)) != 0) && (i < 1000)) {
		i++;
	}

	mdstat = CSL_PSC_getModuleState(mdstat_emac_module_num);
	/* report result. */
	if (mdstat != PSC_MODSTATE_ENABLE) {
	   return 1; /* Could not enable the PSC Module */
	}
#endif
	CSL_BootCfgGetMacIdentifier(&mac_addr1, &mac_addr2);

	/* Reset MAC */
	EMAC_REGS->SOFTRESET = 0x01;
	while(EMAC_REGS->SOFTRESET != 0);

	/* Reset MAC Control */
	EMAC_REGS->MACCONTROL = 0x0;

	/* Init HDPs to NULL */
	pRegAddr = &EMAC_REGS->TX0HDP;
	for(i = 0; i < 8; i++)
		*pRegAddr++ = 0;

	pRegAddr = &EMAC_REGS->RX0HDP;
	for(i = 0; i < 8; i++)
		*pRegAddr++ = 0;

	/* Init CPs to NULL */
	pRegAddr = &EMAC_REGS->TX0CP;
	for(i = 0; i < 8; i++)
		*pRegAddr++ = 0;

	pRegAddr = &EMAC_REGS->RX0CP;
	for(i = 0; i < 8; i++)
		*pRegAddr++ = 0;

	/* Init MAC Addresses */
	for(i = 0; i < 32; i++)
	{
		EMAC_REGS->MACINDEX = i;
		EMAC_REGS->MACADDRHI = 0;
		EMAC_REGS->MACADDRLO = 0;
	}

	/* Setup MAC Address for Channel 0 */
	EMAC_REGS->MACINDEX = 0;
	EMAC_REGS->MACADDRHI = ((mac_addr1 & 0x00ff0000) << 8)
			| ((mac_addr1 & 0xff000000) >> 8)
			| ((mac_addr2 & 0x000000ff) << 8)
			| ((mac_addr2 & 0x0000ff00) >> 8);

	EMAC_REGS->MACADDRLO = CSL_FMK(EMAC_MACADDRLO_VALID, 1)
			| CSL_FMK(EMAC_MACADDRLO_MATCHFILT, 1)
			| ((mac_addr1 & 0x000000ff) << 8)
			| ((mac_addr1 & 0x0000ff00) >> 8);

	/* Setup MAC Address for Channel 0 */
	EMAC_REGS->MACINDEX = 0;
	EMAC_REGS->MACADDRHI = ((mac_addr1 & 0x00ff0000) << 8)
			| ((mac_addr1 & 0xff000000) >> 8)
			| ((mac_addr2 & 0x000000ff) << 8)
			| ((mac_addr2 & 0x0000ff00) >> 8);

	EMAC_REGS->MACADDRLO = CSL_FMK(EMAC_MACADDRLO_VALID, 1)
			| CSL_FMK(EMAC_MACADDRLO_MATCHFILT, 1)
			| ((mac_addr1 & 0x000000ff) << 8)
			| ((mac_addr1 & 0x0000ff00) >> 8);

	printf("macaddress: %02X:%02X:%02X:%02X:%02X:%02X\n",
			((mac_addr2 & 0x0000ff00) >> 8),
			(mac_addr2 & 0x000000ff),
			((mac_addr1 & 0xff000000) >> 24),
			((mac_addr1 & 0x00ff0000) >> 16),
			((mac_addr1 & 0x0000ff00) >> 8),
			(mac_addr1 & 0x000000ff));

	EMAC_REGS->RXBUFFEROFFSET = 0;

	EMAC_REGS->RXMBPENABLE = 0;
	EMAC_REGS->MACHASH1 = 0;
	EMAC_REGS->MACHASH2 = 0;

	/* Clear Unicast RX on channel 0-7 */
	EMAC_REGS->RXUNICASTCLEAR = 0xff;

	/* Disable all interrupts */
	EMAC_REGS->RXINTMASKCLEAR = 0xFF;
	EMAC_REGS->TXINTMASKCLEAR = 0xFF;
	EMAC_REGS->MACINTMASKCLEAR = 0x0;

	/* Set TX descriptor address */
	/* Use CPPI Ram for buffer descriptor and buffer */
	emac_mcb.tx_desc = (EMAC_Desc *)TX_DESC_ADD(0);
	/* setup TX descriptor */
	emac_mcb.tx_desc->pNext = 0;
	emac_mcb.tx_desc->pBuffer = 0;
	emac_mcb.tx_desc->BufOffLen = 0;
	emac_mcb.tx_desc->PktFlgLen = 0;

	/* Set RX descriptor address */
	/* Use CPPI Ram for buffer descriptor and buffer */
	emac_mcb.rx_desc = (EMAC_Desc *)RX_DESC_ADD(0);
	/* setup RX descriptor */
	emac_mcb.rx_desc->pNext = 0;
	emac_mcb.rx_desc->pBuffer = (Uint8 *)RX_BUFF_ADD(0);
	emac_mcb.rx_desc->BufOffLen = RX_BUFF_SIZE;
	emac_mcb.rx_desc->PktFlgLen = EMAC_DSC_FLAG_OWNER;

	/* Enable RX and TX for channel 0 */
	EMAC_REGS->TXCONTROL = 0x01;
	EMAC_REGS->RXCONTROL = 0x01;

	EMAC_REGS->MACCONTROL = ( 1 << 18)	/* EXT_EN */
			| ( 0 << 9 )			/* Round robin */
			| ( 1 << 7 )            /* GIG */
			| ( 0 << 6 )            /* TX pacing disabled */
			| ( 1 << 5 )            /* GMII RX & TX */
			| ( 0 << 4 )            /* TX flow disabled */
			| ( 0 << 3 )            /* RX flow disabled */
			| ( 0 << 1 )            /* Loopback enabled */
			| ( 1 << 0 );           /* full duplex */

	/* Start RX for channel 0 */
	EMAC_REGS->RX0HDP = (Uint32) emac_mcb.rx_desc;
	emac_mcb.lastrxhdp = (Uint32) emac_mcb.rx_desc;

	/* Enable RX Filter for Channel 0 */
	EMAC_REGS->RXUNICASTSET = 0x01;

	/* Enable receive for all broadcast packet */
	EMAC_REGS->RXMBPENABLE = CSL_FMK(EMAC_RXMBPENABLE_RXBROADEN, 1);

	return 0;
}

Int32 EMAC_Send(Uint8 *buff, int len)
{
	volatile EMAC_Desc *pDesc;

	/* minimum 64 bytes required */
	if (len < 64)
		len = 64;

	/* setup descriptor for transmission */
	emac_mcb.tx_desc->pNext = 0;
	emac_mcb.tx_desc->BufOffLen = len;
	emac_mcb.tx_desc->pBuffer = (Uint8 *)TX_BUFF_ADD;
	memcpy(emac_mcb.tx_desc->pBuffer, buff, len);
	emac_mcb.tx_desc->PktFlgLen = EMAC_DSC_FLAG_SOP | EMAC_DSC_FLAG_EOP | EMAC_DSC_FLAG_OWNER | len;

	/* send packet */
	EMAC_REGS->TX0HDP = (Uint32)emac_mcb.tx_desc;

	/* wait for TX complete */
	do {
		pDesc = (EMAC_Desc *)EMAC_REGS->TX0CP;
	} while(pDesc->PktFlgLen & EMAC_DSC_FLAG_OWNER);

	/* return no. of bytes transmitted */
	return len;
}

Int32 EMAC_Recv(Uint8 *buff)
{
	EMAC_Desc *pDesc;
	int recv_bytes = 0;

	/* check for new packet */
	if(emac_mcb.lastrxhdp == (Uint32)EMAC_REGS->RX0HDP)
		return 0;

	pDesc = (EMAC_Desc *)EMAC_REGS->RX0CP;
	if (pDesc->PktFlgLen & EMAC_DSC_FLAG_SOP) {
		/* Acknowledge recevied packet */
		EMAC_REGS->RX0CP = (Uint32)pDesc;

		/* store bytes recevied */
		recv_bytes = pDesc->PktFlgLen & 0xFFFF;

		/* copy data to output buffer */
		memcpy(buff, pDesc->pBuffer, recv_bytes);

		/* re-initalize descriptor to recevie more data */
		pDesc->BufOffLen = RX_BUFF_SIZE;
		pDesc->PktFlgLen = EMAC_DSC_FLAG_OWNER;

		/* assign descriptor to HDP */
		EMAC_REGS->RX0HDP = (Uint32)pDesc;
	}

	/* return number of bytes received */
	return recv_bytes;
}
#endif
