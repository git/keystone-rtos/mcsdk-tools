/******************************************************************************
 * Copyright (c) 2011-2012 Texas Instruments Incorporated - http://www.ti.com
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

/******************************************************************************
 *
 * File Name:  	evm665x_sgmii.c
 *
 * Description:	This file contains the SGMII initialization API
 *
 ******************************************************************************/

#ifdef _EVMC6657L_
#include "platform.h"
#include <ti/csl/csl_cpsgmii.h>
/************************
 * Defines and Macros
 ************************/

#ifndef CSL_EMAC_0_REGS
#define CSL_EMAC_0_REGS			(CSL_EMAC_SS_CFG_REGS)
#endif

#ifndef CSL_MDIO_0_REGS
#define CSL_MDIO_0_REGS			(CSL_EMAC_SS_CFG_REGS + 0x0800)
#endif

#ifndef CSL_SGMII_0_REGS
#define CSL_SGMII_0_REGS		(CSL_EMAC_SS_CFG_REGS + 0x0900)
#endif

#define EMAC_RAM_BASE			(CSL_EMAC_SS_CFG_REGS + 0x2000)
#define EMAC_RAM_LEN      		(0x00002000u)

/* MTU Size */
#define MAX_MTU_SIZE	1536

/* TX & RX Descriptor setup */
#define DESC_SIZE		sizeof(EMAC_Desc)
#define TX_DESC_START	EMAC_RAM_BASE
#define TX_DESC_COUNT	1
#define TX_DESC_ADD(x)	(TX_DESC_START + (DESC_SIZE * x))
#define TX_DESC_END		(TX_DESC_START + (DESC_SIZE * TX_DESC_COUNT))
#define RX_DESC_START	TX_DESC_END
#define RX_DESC_COUNT	1
#define RX_DESC_ADD(x)	(RX_DESC_START + (DESC_SIZE * x))
#define RX_DESC_END		(RX_DESC_START + (DESC_SIZE * RX_DESC_COUNT))
#define RX_BUFF_SIZE	MAX_MTU_SIZE
#define RX_BUFF_START	RX_DESC_END
#define RX_BUFF_ADD(x)	(RX_BUFF_START + (RX_BUFF_SIZE * x))
#define RX_BUFF_END		(RX_BUFF_START + (RX_BUFF_SIZE * RX_DESC_COUNT))
#define TX_BUFF_ADD		RX_BUFF_END


#define SGMII_SERDES_STS			(*(unsigned int*)(CSL_BOOT_CFG_REGS + 0x158))


static SGMII_delay(int delay)
{
	while(--delay);
}

Uint32 SGMII_reset ()
{
	int timeout = 10000;
	CSL_FINS(SGMII_REGS->SOFT_RESET, SGMII_SOFT_RESET_SOFT_RESET, 1);

	while(--timeout && CSL_FEXT(SGMII_REGS->SOFT_RESET, SGMII_SOFT_RESET_SOFT_RESET));

	return 0;
}

Uint32 SGMII_config (SGMII_Config *config)
{
	int timeout;

	if(!config)
		return SGMII_ERROR_INVALID;

	timeout = 10000;
	while(--timeout && !(SGMII_REGS->STATUS & SGMII_STATUS_LOCK))
		SGMII_delay(1000);

	SGMII_REGS->CONTROL = 0;
	if(config->loopbackEn) {
		CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MR_AN_ENABLE, 0);
		CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MASTER, 1);
		CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_LOOPBACK, config->loopbackEn);
	} else {
		CSL_FINS(SGMII_REGS->SOFT_RESET, SGMII_SOFT_RESET_RT_SOFT_RESET, 1);
		if((config->masterEn) && (config->modeOfOperation == SGMII_MODE_OF_OPERATION_WITH_AN)) {
			CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MASTER, 1);
			CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MR_AN_ENABLE, 1);
			SGMII_REGS->MR_ADV_ABILITY = 0x01;
		} else if((config->masterEn) && (config->modeOfOperation == SGMII_MODE_OF_OPERATION_WITHOUT_AN)) {
			CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MASTER, 1);
			CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MR_AN_ENABLE, 0);
			SGMII_REGS->MR_ADV_ABILITY = 0x9801;
		} else {
			CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MASTER, 0);
			CSL_FINS(SGMII_REGS->CONTROL, SGMII_CONTROL_MR_AN_ENABLE, 1);
			SGMII_REGS->MR_ADV_ABILITY = 0x1;
		}
		CSL_FINS(SGMII_REGS->SOFT_RESET, SGMII_SOFT_RESET_RT_SOFT_RESET, 0);
	}

	SGMII_REGS->TX_CFG = config->txConfig;
	SGMII_REGS->RX_CFG = config->rxConfig;
	SGMII_REGS->AUX_CFG = config->auxConfig;

	return 0;
}

Uint32 SGMII_getStatus (SGMII_Status *pStatus)
{
	if(!pStatus)
		return SGMII_ERROR_INVALID;

	pStatus->txCfgStatus = SGMII_REGS->TX_CFG;
	pStatus->rxCfgStatus = SGMII_REGS->RX_CFG;
	pStatus->auxCfgStatus = SGMII_REGS->AUX_CFG;

	return 0;
}

Uint32 SGMII_getLinkStatus ()
{
	int timeout = 1000;
	while(--timeout && !(SGMII_REGS->STATUS & SGMII_STATUS_LOCK))
		SGMII_delay(1000);
	if(!timeout)
		return SGMII_ERROR_DEVICE;

	timeout = 1000;
	while(--timeout && !(timeout & SGMII_STATUS_LINK)) {
		SGMII_delay(1000);
	}

	if(SGMII_REGS->STATUS & SGMII_STATUS_LINK)
		return 1;
	else
		return 0;
}

Uint32 SGMII_getLinkPartnerStatus ()
{
	int timeout = 1000;
	while(--timeout && !(SGMII_REGS->STATUS & SGMII_STATUS_LOCK))
		SGMII_delay(1000);
	if(!timeout)
		return SGMII_ERROR_DEVICE;

	if(SGMII_REGS->STATUS & SGMII_STATUS_MR_AN_COMPLTE)
		return 1;
	else
		return 0;
}

Uint32 SGMII_getAnErrorStatus ()
{
	int timeout = 1000;
	while(--timeout && !(SGMII_REGS->STATUS & SGMII_STATUS_LOCK))
		SGMII_delay(1000);
	if(!timeout)
		return SGMII_ERROR_DEVICE;

	if(SGMII_REGS->STATUS & SGMII_STATUS_AN_ERROR)
		return 1;
	else
		return 0;
}

Uint32 SGMII_getStatusReg ()
{
	return SGMII_REGS->STATUS;
}

Uint32 sgmii_init()
{
	SGMII_Config sgmii_config;

	sgmii_config.loopbackEn = 0;
	sgmii_config.masterEn = 0;
	sgmii_config.modeOfOperation = SGMII_MODE_OF_OPERATION_WITH_AN;

	sgmii_config.txConfig = 0x108A1;
	sgmii_config.rxConfig = 0x00700611;
	sgmii_config.auxConfig = 0x51;

	SGMII_reset();

	SGMII_REGS->CONTROL = 0;

	SGMII_config(&sgmii_config);

	/* link status */
	return !SGMII_getLinkStatus();
}
#endif
