/*
 *
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/



/************************************************************************************
 * FILE PURPOSE: C6678 Device Specific functions
 ************************************************************************************
 * FILE NAME: c6678.c
 *
 * DESCRIPTION: Implements the device specific functions for the IBL
 *
 * @file c6678.c
 *
 * @brief
 *  This file implements the device specific functions for the IBL
 *
 ************************************************************************************/
#include <string.h>
#include "platform.h"
#include "qm_api.h"
#include "cpdma_api.h"
#include "pa_api.h"
#include "net.h"
#include "target.h"

extern cregister unsigned int DNUM;

/***********************************************************************************
 * FUNCTION PURPOSE: Provide an approximate delay
 ***********************************************************************************
 * DESCRIPTION: A delay in units of CPU cycles is executed
 ***********************************************************************************/
void chipDelay32 (uint32_t del)
{
    volatile unsigned int i;

    for (i = 0; i < del/8; i++);

}

void *iblMemcpy (void *s1, const void *s2, uint32_t n)
{
    return (memcpy (s1, s2, n));

}

/**
 *  @brief Determine if an address is local
 *
 *  @details
 *    Examines an input address to determine if it is a local address. Using the largest
 *    L2 size on the 6616.
 */
bool address_is_local (uint32_t addr)
{
    /* L2 */
    if ((addr >= 0x00800000) && (addr < 0x00900000))
        return (TRUE);

    /* L1P */
    if ((addr >= 0x00e00000) && (addr < 0x00e08000))
        return (TRUE);

    /* L2D */
    if ((addr >= 0x00f00000) && (addr < 0x00f08000))
        return (TRUE);

    return (FALSE);

}


/**
 * @brief  Convert a local l1d, l1p or l2 address to a global address
 *
 * @details
 *  The global address is formed. If the address is not local then
 *  the input address is returned
 */
uint32_t deviceLocalAddrToGlobal (uint32_t addr)
{

    if (address_is_local (addr))
        addr = (1 << 28) | (DNUM << 24) | addr;

    return (addr);

}



/**
 *  @brief
 *    The e-fuse mac address is loaded
 */
void deviceLoadDefaultEthAddress (uint8_t *maddr)
{
    uint32_t macA, macB;

    /* Read the e-fuse mac address */
    macA = *((uint32_t *)0x2620110);
    macB = *((uint32_t *)0x2620114);

    maddr[0] = (macB >>  8) & 0xff;
    maddr[1] = (macB >>  0) & 0xff;
    maddr[2] = (macA >> 24) & 0xff;
    maddr[3] = (macA >> 16) & 0xff;
    maddr[4] = (macA >>  8) & 0xff;
    maddr[5] = (macA >>  0) & 0xff;
}


/**
 *  @brief
 *    Compile time queue manager information
 */
#define DEVICE_NUM_RX_CPPIS     1
#define DEVICE_NUM_TX_CPPIS     1
#define DEVICE_NUM_CPPIS        (DEVICE_NUM_RX_CPPIS + DEVICE_NUM_TX_CPPIS)

/* The linking RAM */
#pragma DATA_SECTION(qm_linkram_buf, ".linkram")
#pragma DATA_ALIGN(qm_linkram_buf, 16)
uint8_t qm_linkram_buf[DEVICE_NUM_CPPIS * 2 * (sizeof(uint32_t)/sizeof(uint8_t))];


/* The CPPI RAM */
#pragma DATA_SECTION(qm_cppi_buf, ".cppi")
#pragma DATA_ALIGN(qm_cppi_buf, 16)
uint8_t qm_cppi_buf[QM_DESC_SIZE_BYTES * DEVICE_NUM_CPPIS];

/* The rx data buffers */
#pragma DATA_SECTION(qm_buffer, ".mac_buffer")
#pragma DATA_ALIGN(qm_buffer, 16)
uint8_t qm_buffer[MAX_SIZE_STREAM_BUFFER * DEVICE_NUM_RX_CPPIS];

const qmConfig_t qmConfig =  {
    (uint32_t) qm_linkram_buf,
    sizeof  (qm_cppi_buf),
    (uint32_t) qm_cppi_buf,

    DEVICE_NUM_CPPIS,
    DEVICE_QM_FREE_Q
};

/**
 *  @brief
 *      Return the queue manager memory configuration information
 */
void *targetGetQmConfig (void)
{
    return ((void *)&qmConfig);
}

/**
 *  @brief
 *      Attach a packet buffer to each descriptor and push onto the linked buffer queue
 */
void targetInitQs (void)
{
    int32_t i;
    qmHostDesc_t *hd, *hd_old;

    for (i = 0; i < DEVICE_NUM_RX_CPPIS; i++)  {

        hd                = hwQmQueuePop (DEVICE_QM_FREE_Q);
        hd_old            = hd;
        hd->buffLen       = sizeof (qm_buffer) / DEVICE_NUM_CPPIS;
        hd->buffPtr       = (uint32_t) &(qm_buffer[MAX_SIZE_STREAM_BUFFER * i]);
        hd->nextBDPtr     = 0;
        hd->origBufferLen = MAX_SIZE_STREAM_BUFFER;
        hd->origBuffPtr   = hd->buffPtr;

        hwQmQueuePush (hd, DEVICE_QM_LNK_BUF_Q, QM_DESC_SIZE_BYTES);

    }


    for (i = 0; i < DEVICE_NUM_TX_CPPIS; i++)  {

        hd                = hwQmQueuePop (DEVICE_QM_FREE_Q);
        if (hd == hd_old)
        	hd += 0x40;
        hd->buffLen       = 0;
        hd->buffPtr       = 0;
        hd->nextBDPtr     = 0;
        hd->origBufferLen = 0;
        hd->origBuffPtr   = 0;

        hwQmQueuePush (hd, DEVICE_QM_TX_Q, QM_DESC_SIZE_BYTES);

    }


}



const cpdmaRxCfg_t cpdmaEthRxCfg =  {

    DEVICE_PA_CDMA_RX_CHAN_CFG_BASE,    /* Base address of PA CPDMA rx config registers */
    DEVICE_PA_CDMA_RX_NUM_CHANNELS,     /* Number of rx channels */

    DEVICE_PA_CDMA_RX_FLOW_CFG_BASE,    /* Base address of PA CPDMA rx flow registers */
    DEVICE_PA_CDMA_RX_NUM_FLOWS,        /* Number of rx flows */

    0,                                  /* Queue manager for descriptor / buffer for received packets */
    DEVICE_QM_LNK_BUF_Q,                /* Queue of descriptors /buffers for received packets */

    0,                                  /* Queue manager for received packets */
    DEVICE_QM_RCV_Q,                    /* Queue for received packets (overridden by PA)  */

    DEVICE_RX_CDMA_TIMEOUT_COUNT        /* Teardown maximum loop wait */
};


/**
 *  @brief
 *      Return the cpdma configuration information
 */
void *targetGetCpdmaRxConfig (void)
{
    return ((void *)&cpdmaEthRxCfg);

}


const cpdmaTxCfg_t cpdmaEthTxCfg = {

    DEVICE_PA_CDMA_GLOBAL_CFG_BASE,     /* Base address of global config registers      */
    DEVICE_PA_CDMA_TX_CHAN_CFG_BASE,    /* Base address of PA CPDMA tx config registers */
    DEVICE_PA_CDMA_TX_NUM_CHANNELS      /* Number of tx channels */

};


/**
 *  @brief
 *      return the tx cpdma configuration information
 */
void *targetGetCpdmaTxConfig (void)
{
    return ((void *)&cpdmaEthTxCfg);

}

/**
 *  @brief
 *     Configure the PA
 */
void targetPaConfig (uint8_t *macAddr)
{
    paConfig_t     paCfg;
    qmHostDesc_t  *hd;
    int16_t         ret;

    /* Filter everything except the desired mac address and the broadcast mac */
    paCfg.mac0ms = ((uint32_t)macAddr[0] << 24) | ((uint32_t)macAddr[1] << 16) | ((uint32_t)macAddr[2] << 8) | (uint32_t)(macAddr[3]);
    paCfg.mac0ls = ((uint32_t)macAddr[4] << 24) | ((uint32_t)macAddr[5] << 16);

    paCfg.mac1ms = 0xffffffff;
    paCfg.mac1ls = 0xffff0000;

    paCfg.rxQnum = DEVICE_QM_RCV_Q;

    /* Form the configuration command in a buffer linked to a descriptor */
    hd = hwQmQueuePop (DEVICE_QM_LNK_BUF_Q);
    paCfg.cmdBuf = (uint8_t *)hd->origBuffPtr;

    ret = hwPaEnable (&paCfg);
    if (ret != 0)  {
        return;
    }


    /* Send the command to the PA through the QM */
    hd->softwareInfo0 = PA_MAGIC_ID;
    hd->buffLen = 16;
    QM_DESC_DESCINFO_SET_PKT_LEN(hd->descInfo, 16);

    /* Set the return Queue */
    QM_DESC_PINFO_SET_QM    (hd->packetInfo, 0);
    QM_DESC_PINFO_SET_QUEUE (hd->packetInfo, DEVICE_QM_LNK_BUF_Q);

    hwQmQueuePush (hd, DEVICE_QM_PA_CFG_Q, QM_DESC_SIZE_BYTES);


}

int32_t targetMacSend (void *vptr_device, uint8_t* buffer, int32_t num_bytes)
{
    qmHostDesc_t   *hd;
    int32_t        i;


    /* Must always setup the descriptor to have the minimum packet length */
    if (num_bytes < 64)
        num_bytes = 64;


    for (i = 0, hd = NULL; hd == NULL; i++, chipDelay32 (1000))
        hd = hwQmQueuePop (DEVICE_QM_TX_Q);

    if (hd == NULL)
        return (-1);

    QM_DESC_DESCINFO_SET_PKT_LEN(hd->descInfo, num_bytes);

    hd->buffLen       = num_bytes;
    hd->origBufferLen = num_bytes;

    hd->buffPtr     = deviceLocalAddrToGlobal((uint32_t)buffer);
    hd->origBuffPtr = deviceLocalAddrToGlobal((uint32_t)buffer);


    /* Return the descriptor back to the transmit queue */
    QM_DESC_PINFO_SET_QM(hd->packetInfo, 0);
    QM_DESC_PINFO_SET_QUEUE(hd->packetInfo, DEVICE_QM_TX_Q);
    /* Force the packet to EMAC port 0 if loopback is enabled */
    hd->packetInfo |= (1<<16);

    hwQmQueuePush (hd, DEVICE_QM_ETH_TX_Q, QM_DESC_SIZE_BYTES);

    return (0);

}


int32_t targetMacRcv (void *vptr_device, uint8_t *buffer)
{
    int32_t           pktSizeBytes;
    qmHostDesc_t   *hd;

    hd = hwQmQueuePop (DEVICE_QM_RCV_Q);
    if (hd == NULL)
        return (0);

    pktSizeBytes = QM_DESC_DESCINFO_GET_PKT_LEN(hd->descInfo);
    iblMemcpy ((void *)buffer, (void *)hd->buffPtr, pktSizeBytes);

    hd->buffLen = hd->origBufferLen;
    hd->buffPtr = hd->origBuffPtr;

    hwQmQueuePush (hd, DEVICE_QM_LNK_BUF_Q, QM_DESC_SIZE_BYTES);

    return (pktSizeBytes);

}

void targetFreeQs (void)
{
    qmHostDesc_t   *hd;

    do  {

        hd = hwQmQueuePop (DEVICE_QM_FREE_Q);

    } while (hd != NULL);

    do  {

        hd = hwQmQueuePop (DEVICE_QM_LNK_BUF_Q);

    } while (hd != NULL);

    do  {

        hd = hwQmQueuePop (DEVICE_QM_RCV_Q);

    } while (hd != NULL);

    do  {

        hd = hwQmQueuePop (DEVICE_QM_TX_Q);

    } while (hd != NULL);

}











